
/**
 * Write a description of class AudioList here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.util.Scanner;
import java.io.File;
import java.util.List;
import java.util.ArrayList;
 
public class AudioList
{
    private static String path = "D:/Magister Life/Sem 2/TD RK/recorder-with-bluej";
    
    private File folder = new File(path);
    private File[] listOfFiles = folder.listFiles();
    private List<File> listofAudio = new ArrayList<File>();
    private Scanner scan = new Scanner(System.in);
    private File audio;
    private String files;
    
    public AudioList()
    {
      for (int i = 0; i < listOfFiles.length; i++) 
      {
           if (listOfFiles[i].isFile()) 
           {
               files = listOfFiles[i].getName();
               if (files.endsWith(".wav") || files.endsWith(".WAV"))
               {
                  listofAudio.add(listOfFiles[i]);
               }
           }
      }
    }
    
    public int printlistofAudio()
    {
      System.out.print("\u000C");
      int tempIndex = 0;
      System.out.println("List of Audio:");
      for (int i = 0; i < listofAudio.size(); i++)
      {
          tempIndex = i + 1;
          files = tempIndex + ". " + listofAudio.get(i).getName();
          System.out.println(files);
      }
      System.out.print("Type audio's index number to play or delete OR type 0 to back to main menu: ");
      return scan.nextInt();
    }
    
    public String getAudioName(int index)
    {
      try
      {
         audio = listofAudio.get(index);
         return audio.getName();
      }
      catch(IndexOutOfBoundsException e)
      {
         return "Audio not found"; 
      }  
    }
    
    public void deleteAudio()
    {
      if(audio.delete()){
          System.out.println(audio.getName() + " is deleted!");
      }else{
          System.out.println("Delete operation is failed.");
      }
    }
}
