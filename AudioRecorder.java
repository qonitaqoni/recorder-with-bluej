
/**
 * Write a description of class AudioRecorder here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

import java.io.IOException;
import java.io.File;

import javax.sound.sampled.DataLine;
import javax.sound.sampled.TargetDataLine;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.AudioFileFormat;

public class AudioRecorder extends Thread
{
    private TargetDataLine line;
    private AudioFileFormat.Type targetType;
    private AudioInputStream audioInputStream;
    private File outputFile;

    public AudioRecorder(String filename)
    {   
        System.out.print("\u000C");
        outputFile = new File(filename);

        AudioFormat audioFormat = new AudioFormat(
            AudioFormat.Encoding.PCM_SIGNED,
            44100.0F, 16, 2, 4, 44100.0F, false);
            
        DataLine.Info info = new DataLine.Info(TargetDataLine.class, audioFormat);
        
        try
        {
            line = (TargetDataLine) AudioSystem.getLine(info);
            line.open(audioFormat);
        }
        catch (LineUnavailableException e)
        {
            System.out.println("Unable to get a recording line");
            e.printStackTrace();
        }

        targetType = AudioFileFormat.Type.WAVE;
        audioInputStream = new AudioInputStream(line);
        
        System.out.println("Now Recording: " + filename);
        System.out.println("Press ENTER to start the recording.");
        try
        {
            System.in.read();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        
        start();
        System.out.println("Recording...");
        
        System.out.println("Press ENTER to stop the recording.");
        try
        {
            System.in.read();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        stopRecording();
        System.out.println("Recording stopped.");
    }

    public void start()
    {
        line.start();
        super.start();
    }

    private void stopRecording()
    {
        line.stop();
        line.close();
    }

    public void run()
    {
        try
        {
            AudioSystem.write(
                audioInputStream,
                targetType,
                outputFile);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }    
    }
}
