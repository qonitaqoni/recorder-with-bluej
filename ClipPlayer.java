
/**
 * Write a description of class ClipPlayer here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;

public class ClipPlayer
extends Thread
implements LineListener
{
    private Clip m_clip;
    private File m_inputFile;
    private int lastFrame;
    private String choice;
    private boolean stopped = false;

    public ClipPlayer(String filename)
    {
        Scanner scan = new Scanner(System.in);
        m_inputFile = new File(filename);
            
        play();
        System.out.println("Playing...");
            
        while(!stopped)
        {
            System.out.print("\u000C");
            System.out.println("Audio Loaded: " + filename);
            System.out.println("Type p to pause playback.\nType r to resume playback.\nType s to stop and go to beginning of playback.\nType e to exit.");
            choice = scan.nextLine();
            readInput(choice.trim()); 
        }
        
        m_clip.close();
        System.gc();
    }

    private void play()
    {
        AudioInputStream audioInputStream = null;
        try
        {
            audioInputStream = AudioSystem.getAudioInputStream(m_inputFile);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        
        if (audioInputStream != null)
        {
            AudioFormat format = audioInputStream.getFormat();
            DataLine.Info info = new DataLine.Info(Clip.class, format);
            try
            {
                m_clip = (Clip) AudioSystem.getLine(info);
                m_clip.addLineListener(this);
                m_clip.open(audioInputStream);
            }
            catch (LineUnavailableException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            super.start();
        }
        else
        {
            System.out.println("Can't get data from file " + m_inputFile.getName());
        }
    }
    
    public void run()
    {
        m_clip.loop(0);
    }

    public void update(LineEvent event)
    {
        if (event.getType().equals(LineEvent.Type.START))
        {
            System.out.println("Started");
        }
        else if (event.getType().equals(LineEvent.Type.STOP))
        {
            lastFrame = m_clip.getFramePosition();
            if (lastFrame < m_clip.getFrameLength()) 
            {
                m_clip.setFramePosition(lastFrame);
            } else {
                m_clip.setFramePosition(0);
            }
            System.out.println("Stopped");
        }
        else if (event.getType().equals(LineEvent.Type.CLOSE))
        {
            System.out.println("Closing player...");
        }

    }
   
    private void readInput(String choices)
    {
        switch(choices)
        {
                case "p": case "P":
                    if (m_clip.isRunning()) 
                    {
                        m_clip.stop();
                    }
                    break;
                case "s": case "S":
                    m_clip.setFramePosition(0);
                    m_clip.stop();
                    break;
                case "r": case "R":
                    if (!m_clip.isRunning()) 
                    {
                        m_clip.start();
                    }
                    break;
                case "e": case "E":
                    stopped = true;
                    break;
                default:
                    System.out.println("Wrong Input");
                    try {
                        Thread.sleep(500);
                    }
                    catch(InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                    break;
        }
    }
}
