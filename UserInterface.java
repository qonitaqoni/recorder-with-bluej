
/**
 * Write a description of class UserInterface here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */

import java.util.Scanner;

public class UserInterface extends Thread
{
    private int input;
    private int index;
    private String filename;
    private Scanner scan = new Scanner(System.in);
    
    public static void main( String[] args ) 
    {
        UserInterface UI = new UserInterface();
        UI.mainMenu();
    }
    
    private void mainMenu()
    {
        System.out.print("\u000C");
        out("DIGITAL AUDIO RECORDER");
        out("-----------------------");
        out("1. Record audio");
        out("2. List of audio recorded");
        out("3. Exit");
        input = in("Type your choice(1/2/3): ");
        switch(input)
        {
            case 1:
                while(true){
                    System.out.print("Filename(type .wav at the end of file name): ");
                    filename = scan.next();
                    if(!filename.toLowerCase().contains(".wav")){
                        out("Format not recognized");
                    }
                    else{
                        break;
                    }
                }
                AudioRecorder audioRecorder = new AudioRecorder(filename.trim());
                try {
                    Thread.sleep(1000);
                }
                catch(InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
                mainMenu();
                break;
            case 2:
                AudioList audioList = new AudioList();
                getAudioIndex(audioList);
                break;
            case 3:
                out("Closing application...");
                System.exit(0);
            default:
                System.out.println("Wrong Input");
                try {
                    Thread.sleep(500);
                }
                catch(InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
                mainMenu();
                break;
        }
    }
    
    private void audioMenu(AudioList audioList)
    {
        System.out.print("\u000C");
        String audioName = audioList.getAudioName(index);
        out(audioName);
        try {
            Thread.sleep(500);
        }
        catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        
        if(audioName == "Audio not found"){
            getAudioIndex(audioList);
        }
        else{
            out("1. Play audio");
            out("2. Delete audio");
            out("3. Back to list of audio");
            input = in("Type your choice(1/2/3): ");
            switch(input)
            {
                case 1:
                    ClipPlayer clipPlayer = new ClipPlayer(audioName);
                    try {
                        Thread.sleep(1000);
                    }
                    catch(InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                    getAudioIndex(audioList);
                    break;
                case 2:
                    audioList.deleteAudio();
                    try {
                        Thread.sleep(1000);
                    }
                    catch(InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                    AudioList newAudioList = new AudioList();
                    getAudioIndex(newAudioList);
                    break;
                case 3:
                    getAudioIndex(audioList);
                    break;
                default:
                    System.out.println("Wrong Input");
                    try {
                        Thread.sleep(500);
                    }
                    catch(InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                    audioMenu(audioList);
                    break;
            }
        }
    }
    
    private void getAudioIndex(AudioList audioList){
        int tempIndex = audioList.printlistofAudio();
        index = tempIndex - 1;
        if(index == -1){
            mainMenu();
        }
        else{
            audioMenu(audioList);
        }
    }
    
    private static void out(String strMessage)
    {
        System.out.println(strMessage);
    }
    
    private int in(String strMessage)
    {
        System.out.print(strMessage);
        return scan.nextInt();
    }
}
